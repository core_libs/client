package client

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/core_libs/config"
)

const (
	authTypeBasic = "basic"
	authTypeJWT   = "jwt"
)

type restApi struct {
	clients map[string]*coreClient
}

type coreClient struct {
	url        string
	headers    map[string]string
	retryCount int
	client     *http.Client
}

type connection struct {
	URL        string            `json:"url"`
	Key        string            `json:"key"`
	Timeout    int               `json:"timeout"`
	RetryCount int               `json:"retry_count"`
	Auth       *connectionsAuth  `json:"auth"`
	Headers    map[string]string `json:"headers"`
}

type connectionsAuth struct {
	Type string `json:"type"`
	Tag  string `json:"tag"`
}

func NewRestApi(openCfgName, secretCfgName string) (*restApi, error) {
	if openCfgName == "" {
		return nil, fmt.Errorf("ERROR | [core_client] openCfgName is empty")
	}
	openCfg, err := config.GetOpenConfig[[]connection](openCfgName)
	if err != nil {
		return nil, fmt.Errorf("ERROR | [core_client] wrong GetOpenConfig; details - %s", err)
	}

	clients := make(map[string]*coreClient)

	for i := range openCfg {
		if openCfg[i].Auth != nil {
			switch openCfg[i].Auth.Type {
			case authTypeBasic:
				err = createSecretsAuth(openCfg[i].Auth, secretCfgName)
				if err != nil {
					return nil, err
				}
				openCfg[i].Headers["Authorization"] = fmt.Sprintf("Basic %s", openCfg[i].Auth.Tag)
			case authTypeJWT:
				err = createSecretsAuth(openCfg[i].Auth, secretCfgName)
				if err != nil {
					return nil, err
				}
				openCfg[i].Headers["Authorization"] = fmt.Sprintf("Bearer %s", openCfg[i].Auth.Tag)
			default:
				return nil, fmt.Errorf("ERROR | [core_client] wrong auth type [%s]", openCfg[i].Auth.Type)
			}
		}

		err = createSecretsHeaders(&openCfg[i].Headers, secretCfgName)
		if err != nil {
			return nil, err
		}
		timeout := time.Second * time.Duration(openCfg[i].Timeout)
		httpClient := http.Client{Timeout: timeout}
		clients[openCfg[i].Key] = &coreClient{
			url:        openCfg[i].URL,
			headers:    openCfg[i].Headers,
			retryCount: openCfg[i].RetryCount,
			client:     &httpClient,
		}
	}

	return &restApi{
		clients: clients,
	}, nil
}

func createSecretsAuth(connection *connectionsAuth, secretCfgName string) error {
	secretVal, err := config.SetSecret(connection.Tag, secretCfgName)
	if err != nil {
		return err
	}
	connection.Tag = secretVal
	return nil
}

func createSecretsHeaders(connection *map[string]string, secretCfgName string) error {
	for key, val := range *connection {
		if config.CheckHolderFormat(val) {
			secretVal, err := config.SetSecret(val, secretCfgName)
			if err != nil {
				return err
			}
			(*connection)[key] = secretVal
		}
	}
	return nil
}
