# Core Client

!!! _Необходимо будет переехать с `net/http` на [fasthttp](https://github.com/valyala/fasthttp)_

## Конфиги

Основные конфиги для `client` находятся в `open` группе

Пример:
```json
[
  {
    "url": "http://127.0.0.1:8089",
    "key": "telephony",
    "timeout": 5,
    "retry_count": 2,
    "auth": {
      "type": "jwt",
      "tag": "{{ TOKEN }}"
    },
    "headers": {
      "Content-Type": "application/json"
    }
  }
]
```
- `key` - это ключ, по которому будет выдан клиент. Клиентов может быть несколько.
- `auth` - способ авторизации, которые поддерживаем два типа: `basic` и `jwt`. Если авторизация
не нужна - `auth` описывать не нужно
- `headers` - заголовки, с которыми будет отправлен запрос. Заголовки поддерживают секреты.

### Секреты
Секреты - это формат значения, по которому истинное значение будет подставлено из .env переменных
(`secret` группы)

**Формат описания секрета**

Секрет должен начинаться и заканчиваться двумя фигурными скобками. Имя секрета должно быть написано
в верхнем регистре и имя должно совпадать с нужной переменной из `.env` окружения

В примере выше, в ключе `auth` присутствует секрет `{{ TOKEN }}`. Вот так он будет выглядеть в `.env`
окружении:
```
TOKEN: "eyJhbGciOiIXVCJ9.eyJzZXNza3cxMzM0OTk3OH0.E9vM8Fvl93DYpVvM2r-6U"
```

Вот как в данном случае будет выглядеть `conf.json`:
```json
{
  "open": {
    "api": "api"
  },
  "secret": {
    "api": "api"
  }
}
```

## Использование

Для начала, нужно инициализировать нужную api (т.е. набор конфигов)
```go
api, err := rest_api_v2.NewRestApi("api", "api")
if err != nil {
    fmt.Println(err)
    return
}
```
Первым параметром передается имя `open` конфигов, а вторым параметром (опционально) передается
имя `secret` конфигов.

Если секретов нет - необходимо передать пустую строку

Далее, из полученной `api` получаем клиент
```go
cli, err := api.Key("telephony")
if err != nil {
    fmt.Println(err)
    return
}
```
После того, как клиент получен, можно делать запрос

```go
res, err := cli.Get(nil, "web_phone/read/one/reg/v1")
if err != nil {
    fmt.Println(err)
    return
}
```
В методе `Get` первым параметром подаются Query параметрами (тип `Query`, которые представляет из
себя map[string]string), а вторым параметром нужный роут.

Роут можно передать двумя способами:
1.
```go
res, err := cli.Get(nil, "web_phone/read/one/reg/v1")
if err != nil {
    fmt.Println(err)
    return
}
```
2. 
```go
res, err := cli.Get(nil, "web_phone", "read", "one", "reg", "v1")
if err != nil {
    fmt.Println(err)
    return
}
```

Так же, на ходу можно изменять заголовки используя методы `SetHeader` и `DeleteHeader`

Для того, чтобы получить ответ в нужном типе, можно вызвать метод `ReadResponse`

Пример:
```go
type Response struct {
	Status  string      `json:"status"`
	Type    string      `json:"type"`
	Message interface{} `json:"message"`
}
response, err := rest_api_v2.ReadResponse[Response](res)
if err != nil {
    fmt.Println(err)
    return
}
fmt.Println(response)
```