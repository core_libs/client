package client

import (
	"fmt"
	"net/http"
)

type Query map[string]string

type RestApi interface {
	Key(key string) (*coreClient, error)
}

type Client interface {
	Get(params *Query, path ...string) (*http.Response, error)
	Post(params *Query, body []byte, path ...string) (*http.Response, error)
	Put(params *Query, body []byte, path ...string) (*http.Response, error)
	Patch(params *Query, body []byte, path ...string) (*http.Response, error)
	Delete(params *Query, body []byte, path ...string) (*http.Response, error)
	SetHeader(key, value string)
	DeleteHeader(key string)
	PostFormData(params *Query, data map[string]string, path ...string) (*http.Response, error)
}

func (a *restApi) Key(key string) (*coreClient, error) {
	cli, ok := a.clients[key]
	if !ok {
		return nil, fmt.Errorf("key [%s] not found", key)
	}
	return cli, nil
}

func (c *coreClient) Get(params *Query, path ...string) (*http.Response, error) {
	url := c.buildUrl(params, path...)
	var res *http.Response
	var err error
	for i := 0; i < c.retryCount; i++ {
		res, err = c.buildRequest("GET", url, nil)
		if res.StatusCode >= http.StatusInternalServerError {
			continue
		}
		break
	}
	return res, err
}

func (c *coreClient) Post(params *Query, body []byte, path ...string) (*http.Response, error) {
	url := c.buildUrl(params, path...)
	var res *http.Response
	var err error
	for i := 0; i < c.retryCount; i++ {
		res, err = c.buildRequest("POST", url, body)
		if res.StatusCode >= http.StatusInternalServerError {
			continue
		}
		break
	}
	return res, err
}

func (c *coreClient) Put(params *Query, body []byte, path ...string) (*http.Response, error) {
	url := c.buildUrl(params, path...)
	var res *http.Response
	var err error
	for i := 0; i < c.retryCount; i++ {
		res, err = c.buildRequest("PUT", url, body)
		if res.StatusCode >= http.StatusInternalServerError {
			continue
		}
		break
	}
	return res, err
}

func (c *coreClient) Patch(params *Query, body []byte, path ...string) (*http.Response, error) {
	url := c.buildUrl(params, path...)
	var res *http.Response
	var err error
	for i := 0; i < c.retryCount; i++ {
		res, err = c.buildRequest("PATCH", url, body)
		if res.StatusCode >= http.StatusInternalServerError {
			continue
		}
		break
	}
	return res, err
}

func (c *coreClient) Delete(params *Query, body []byte, path ...string) (*http.Response, error) {
	url := c.buildUrl(params, path...)
	var res *http.Response
	var err error
	for i := 0; i < c.retryCount; i++ {
		res, err = c.buildRequest("DELETE", url, body)
		if res.StatusCode >= http.StatusInternalServerError {
			continue
		}
		break
	}
	return res, err
}

func (c *coreClient) SetHeader(key, value string) {
	c.headers[key] = value
}

func (c *coreClient) DeleteHeader(key string) {
	delete(c.headers, key)
}

func (c *coreClient) PostFormData(params *Query, data map[string]string, path ...string) (*http.Response, error) {
	url := c.buildUrl(params, path...)
	var res *http.Response
	var err error
	for i := 0; i < c.retryCount; i++ {
		res, err = c.buildFormDataRequest("POST", url, data)
		if res.StatusCode >= http.StatusInternalServerError {
			continue
		}
		break
	}
	return res, err
}
