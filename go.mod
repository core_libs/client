module gitlab.com/core_libs/client

go 1.22.3

require gitlab.com/core_libs/config v1.0.1

require github.com/joho/godotenv v1.5.1 // indirect
