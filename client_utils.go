package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	urls "net/url"
	"strings"
)

func ReadResponse[T interface{}](response *http.Response) (T, error) {
	defer response.Body.Close()
	var data T
	bodyBytes, err := io.ReadAll(response.Body)
	if err != nil {
		return data, err
	}
	err = json.Unmarshal(bodyBytes, &data)
	if err != nil {
		return data, err
	}

	return data, nil
}

func (c *coreClient) buildHeaders(req *http.Request) {
	for key, value := range c.headers {
		req.Header.Set(key, value)
	}
}

func (c *coreClient) buildUrl(params *Query, path ...string) string {
	baseUrl := c.url
	queryParams := c.buildQueryParams(params)
	if len(path) != 0 {
		for _, p := range path {
			baseUrl += fmt.Sprintf("/%s", p)
		}
	}
	return fmt.Sprintf("%s%s", baseUrl, queryParams)
}

func (c *coreClient) buildQueryParams(params *Query) string {
	if params == nil || len(*params) == 0 {
		return ""
	}

	var buffer bytes.Buffer
	buffer.WriteString("?")

	for key, value := range *params {
		buffer.WriteString(key)
		buffer.WriteString("=")
		buffer.WriteString(value)
		buffer.WriteString("&")
	}

	// Удаление последнего "&"
	return buffer.String()[:buffer.Len()-1]
}

func (c *coreClient) buildRequest(method, url string, body []byte) (*http.Response, error) {
	var req *http.Request
	var err error

	if body == nil || len(body) == 0 {
		req, err = http.NewRequest(method, url, nil)
	} else {
		req, err = http.NewRequest(method, url, bytes.NewBuffer(body))
	}
	if err != nil {
		return nil, err
	}
	c.buildHeaders(req)

	resp, err := c.client.Do(req)
	return resp, nil
}

func (c *coreClient) buildFormDataRequest(method, url string, body map[string]string) (*http.Response, error) {
	var req *http.Request
	var err error
	sendData := urls.Values{}
	for key, val := range body {
		sendData.Set(key, val)
	}

	req, err = http.NewRequest(method, url, strings.NewReader(sendData.Encode()))
	if err != nil {
		return nil, err
	}
	c.buildHeaders(req)

	resp, err := c.client.Do(req)
	return resp, nil
}
